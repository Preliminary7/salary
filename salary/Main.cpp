// Name: Xao Thao
// Date: 10/12/2019
// Project Name: Salary
// Project Description: Enter employee onto a stack or heap and calculate the total employee income 
// then show result of their gross pay and all employee gross pay together


#include <iostream>
#include <conio.h>
#include <string>
#include <vector>

using namespace std;


struct Employee
{
	int id;
	string firstName;
	string lastName;
	float payRate;
	int hours;
	
};


int main()
{


	vector<Employee> employees;
	
	char Another = 'y';

	// Again loop to put as many employee on the stack
	while (Another == 'y' || Another == 'Y')
	{
		Employee e;
		cout << "Enter employee ID: \n";
		cin >> e.id;
		cout << "Enter employee first name: \n";
		cin >> e.firstName;
		cout << "Enter employee last name: \n";
		cin >> e.lastName;
		cout << "Enter employee pay rate: \n";
		cin >> e.payRate;
		cout << "Enter employee hours worked: \n";
		cin >> e.hours;
		employees.push_back(e);

		cout << "Another? (y/n): \n";
		cin >> Another;
		
	}

	

	vector<Employee>::iterator it = employees.begin();
	
	cout << "ID" << "\t" << "First Name" << "\t" << "Last Name" << "\t" << "Pay Rate" << "\t" << "Hours" << "\t" << "Gross Pay" << "\n";

	float sum = 0;

	for (; it != employees.end(); it++)
	{
		float grossPay = it->payRate * it->hours; // equation

		cout << it->id << "\t" << it->firstName << "\t\t" << it->lastName << "\t\t"
			<< it->payRate << "\t\t" << it->hours << "\t" << "$" << grossPay << "\n"; // output the information of employee as a row 
		sum += grossPay; // add the sum of all employees and shows the gross pay
	}

	cout << "The gross pay of all employees: " << "$" << sum;

	_getch();
	return 0;

}